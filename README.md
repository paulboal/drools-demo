Purpose
===

This is simply a demonstration that we're using to show how we can read Drools templates and parameters from a String (ultimately from DynamoDB tables), and apply those against a generalized User Feature object.

See https://www.lucidchart.com/publicSegments/view/c88b05cf-4108-4022-af41-04abfe463cd0/image.png for a picture of how we imagine the data flow working.

