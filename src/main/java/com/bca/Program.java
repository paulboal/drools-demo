package com.bca;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.drools.template.ObjectDataCompiler;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;


public final class Program {
	static Logger log = LogManager.getLogger(EvoUserFeatures.class.getName());

	/**
	 * This a complete Drools template, but all we really need to store in the database is the
	 * WHEN condition.  The THEN action can be derived based on rule name.  Maybe something like:
	 * `modify($f) { addNotification( ${ruleId} ) }.
	 * 
	 * Remember that the notifications may also have placeholders for the user's name or other information.
	 */
	private static String template = "template header\n\n" +
			"dataType\n" +
			"percentOfGoal\n\n" +
			"package com.bca;\n\n" +
			"import com.bca.EvoUserFeatures;\n\n" +
			"template \"notifications\"\n\n" +
			"rule \"evo_notification_@{row.rowNumber}\"\n\n" +
			"when\n" + 
			"    f: EvoUserFeatures(getValue(\"@{dataType}DailyTotal\") > (@{percentOfGoal} * getValue(\"@{dataType}DailyGoal\")))\n" + 
			"then\n" + 
			"    System.out.println( \"Triggered \" + \"@{dataType}\" );\n" + 
			"    f.setTriggered(true);\n" +
			"end\n\n"+
			"end template";

	
	public static void main(String[] args) {
		final Logger log = LogManager.getLogger(Program.class.getName());
		
		
		/**
		 * TODO: The compiler is what we use to turn a template into something that can be executed.
		 * The compiler takes an InputStream (which we can create from the String),
		 * and a list of Map<String,String> that hold the replacement variables, for example:
		 * Map<variable,value> will end up replacing all occurences of @{variable} with value.
		 * That value ends up being dropped into the rule directly, so it can reference a 
		 * method, variable, or be used as a literal value.
		 */
        ObjectDataCompiler compiler = new ObjectDataCompiler();
        
        List<HashMap<String, String>> attributes = new ArrayList<HashMap<String,String>>();
        HashMap<String,String> a = new HashMap<String,String>();
        a.put("dataType", "steps");
        a.put("percentOfGoal", "0.2");
        attributes.add(a);
        log.trace("Loaded attributes");
        log.trace(attributes);
        
        /**
         * TODO: Here, instead of just using a static string, we can load the templates into
         * a Map<int,String> that can be the RuleTemplateID and RuleTemplate.
         */
        log.trace(template);
        log.trace("As Bytes...");
        log.trace(template.getBytes());
        
        InputStream t = new ByteArrayInputStream(template.getBytes(StandardCharsets.UTF_8));
        String drl = compiler.compile(attributes, t);	
        log.trace("Processing " + drl);
        
        log.trace(drl);


        KieServices ks = KieServices.Factory.get();
        KieFileSystem fs = ks.newKieFileSystem();
        fs.write("src/main/resources/rules.drl", drl);
        
        /**
         * TODO: And here's where we fire off the set of rules we have built up against the current
         * EvoUserFeatures object.
         */
        KieBuilder kieBuilder = ks.newKieBuilder( fs ).buildAll();
        Results results = kieBuilder.getResults();
        if (results.hasMessages(Message.Level.ERROR)) {
        	log.error(results.getMessages());
        	throw new IllegalStateException("### errors ###");
        }
        
        KieContainer kContainer = ks.newKieContainer( ks.getRepository().getDefaultReleaseId() );
        
        KieSession kSession = kContainer.newKieSession();
        EvoUserFeatures f = new EvoUserFeatures();
        kSession.insert(f);
        kSession.fireAllRules();
        
        log.info("Final result: " + (f.isTriggered()?"Triggered":"Not"));
    }
}
