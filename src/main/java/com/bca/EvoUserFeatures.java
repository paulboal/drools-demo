package com.bca;

import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;

public class EvoUserFeatures {
	
	static Logger log = LogManager.getLogger(EvoUserFeatures.class.getName());
	
//	private float value = 80;
//	private float goal = 100;
	private boolean triggered = false;
	Map<String,Float> values;

	public EvoUserFeatures() {
		log.trace("Created an EvoUserFeatures");
		values = new HashMap<String,Float>();
		
		values.put("stepsDailyTotal", new Float(80.0));
		values.put("stepsDailyGoal", new Float(100.0));
	}
	
	public float getValue(String keyName) {
		log.trace("getValue(" + keyName + ")");
		return values.get(keyName).floatValue();
	}

//	public float getValue() {
//		log.trace("getValue");
//		return value;
//	}
//
//	public void setValue(float value) {
//		this.value = value;
//	}
//
//	public float getGoal() {
//		log.trace("getGoal");
//		return goal;
//	}
//
//	public void setGoal(float goal) {
//		this.goal = goal;
//	}

	public boolean isTriggered() {
		return triggered;
	}

	public void setTriggered(boolean triggered) {
		log.trace("Set me to true");
		this.triggered = triggered;
	}
	
}
